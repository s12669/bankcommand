package com.maciejsokol.ms;

public class Bank {
    public static void income(double amount, Account account) {
        account.add(amount);
    }

    public void transfer(Account from, Account in, double amount) {
        from.substract(amount);
        in.add(amount);
    }


}
