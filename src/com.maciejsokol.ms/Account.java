package com.maciejsokol.ms;

import java.util.Date;

public class Account {
    private String number;
    private History history;
    private double amount;

    public String getNumber() {
        return number;
    }

    public History getHistory() {
        return history;
    }

    public double getAmount() {
        return amount;
    }

    public Account(String number, History history, double amount) {
        this.number = number;
        this.history = history;
        this.amount = amount;
    }

    public void add(double amount) {
        amount += Income.getAmount();
        Date date = new Date();
        HistoryLog log = new HistoryLog(date, number, new Income());
    }

    public void substract(double amount) {
        amount = this.amount - Transfer.getAmount();
        Date date = new Date();
        HistoryLog log = new HistoryLog(date, number, new Transfer());
    }

    public void doOperation(Operation op) {
        op.execute(this);
    }
}
