package com.maciejsokol.ms;

public enum OperationType {
    INCOME, OUTCOME
}
