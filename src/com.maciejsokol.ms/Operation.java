package com.maciejsokol.ms;


public abstract class Operation {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void execute(Account account);


}
