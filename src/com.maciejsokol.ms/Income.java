package com.maciejsokol.ms;

public class Income extends Operation{
    private static double amount;
    private Account account;


    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public static double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public void execute(Account account) {
        account.add(amount);
    }
}
