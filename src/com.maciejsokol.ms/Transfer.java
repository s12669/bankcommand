package com.maciejsokol.ms;

public class Transfer extends Operation {
    private Account from;
    private Account to;
    private static double amount;

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public Account getTo() {
        return to;
    }

    public void setTo(Account to) {
        this.to = to;
    }

    public static double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public void execute(Account account) {
        account.substract(amount);
    }
}
