package com.maciejsokol.ms;

import java.util.ArrayList;
import java.util.List;

public class History {
    private List<HistoryLog> logs = new ArrayList<HistoryLog>();
    public void log(HistoryLog log) {
        logs.add(log);
    }
}
