package com.maciejsokol.ms;

import java.util.Date;

public class HistoryLog {
    private Date dateOfOperation;
    private String title;
    private Operation operationType;

    public Date getDateOfOperation() {
        return dateOfOperation;
    }

    public String getTitle() {
        return title;
    }

    public Operation getOperationType() {
        return operationType;
    }

    public HistoryLog(Date dateOfOperation, String title, Operation operationType) {
        this.dateOfOperation = dateOfOperation;
        this.title = title;
        this.operationType = operationType;
    }
}
